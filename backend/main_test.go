package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"

	"github.com/alicebob/miniredis"
	"github.com/go-chi/chi"
	redis "github.com/go-redis/redis/v7"
)

func mockRedis() {
	mr, err := miniredis.Run()
	if err != nil {
		panic(err)
	}

	db = redis.NewClient(&redis.Options{
		Addr: mr.Addr(),
	})
}

func initRedis() {
	var colors = []*Color{
		{Name: "red", Hex: "#ff0000"},   // Day: 12
		{Name: "green", Hex: "#008000"}, // Day: 13
		{Name: "blue", Hex: "#0000ff"},
	}

	for _, c := range colors {
		db.RPush("colors", c)
	}
}

func TestGetColor(t *testing.T) {
	mockRedis()
	initRedis()

	// Involve the router in testing, so we get
	// URLParams parsed correctly
	router := chi.NewRouter()
	router.Get("/color/{day}", GetColor)

	response := httptest.NewRecorder()
	var expected string

	// Call GetColor
	request, _ := http.NewRequest("GET", "/color/12", nil)
	router.ServeHTTP(response, request)

	expected = `{"name":"red","hex":"#ff0000"}`

	assert.Equal(t, http.StatusOK, response.Code, "handler returned status code")
	assert.Equal(t, expected, response.Body.String(), "handler returned wrong result")
}
